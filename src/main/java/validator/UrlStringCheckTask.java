package validator;

public class UrlStringCheckTask implements UrlCheckerTask {
	private final String url;
	
	UrlStringCheckTask(String in_url) {
		this.url = in_url;
	}

	@Override
	public float getWeight() {
		return 100.f;
	}

	@Override
	public float getValue() {
		final String ext = this.getExtention();
		if(ext.equals("co") || ext.equals("ru")) {
			return 30.f;
		}
		return 50.f;
	}
	
	String getBaseUrl() {
		String[] strs = this.url.split("/");
		if(strs.length > 0) {
			String noHttp = strs[0];
    		if(strs[0].startsWith("http") && strs.length >= 3 ) {
	    		noHttp = strs[2];
		    }
    		String noWww = noHttp;
    		if(noHttp.startsWith("www")) {
    			noWww = noHttp.substring(4);
    		}
    		String finalString = noWww;
    		return finalString;
		}
		return "";
	}
	
	String getExtention() {
		String[] strs = this.getBaseUrl().split("\\.");
		if(strs.length > 0) {
		    return strs[strs.length-1];
		}
		return "";
	}

}
