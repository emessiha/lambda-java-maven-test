package validator;

public class RandomUrlCheckerTask implements UrlCheckerTask {

	@Override
	public float getWeight() {
		return 30.f;
	}

	@Override
	public float getValue() {
		return getWeight() * (float) Math.random();
	}

}
