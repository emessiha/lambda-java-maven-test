package validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;

/**
 * verifies the rank of list of URLs based on items in the URL database
 * 
 * @author Chris
 *
 */
final class UrlChecker {

	// Input list of URL strings
	private final List<String> urlList;
	
    UrlChecker(List<String> in_urlList) {
    	this.urlList = in_urlList;
    }
    /**
     * gets the rank of the URLs in urlList 
     * @return a list of maps.  each map represents one url and will contain a "url" and "rank" key
     * @throws RankCalculationFailureException when the database access fails
     */
    final List<Map<String,String>> getRank() throws RankCalculationFailureException {
    	final String tableName = "UrlRanks";
		final List<Map<String,String>> outputList = new ArrayList<>();

		// puts each url into a map and adds it the the output list
		for(String url : this.urlList) {
		    final Map<String,String> outputItem = new HashMap<>();
		    outputItem.put("UrlName", url);
		    outputList.add(outputItem);
		}
		
		// Connect to the db and run the query for each url
		BasicAWSCredentials basic = new BasicAWSCredentials("AKIAINLTALKWM6B2TLEA", "3dusQmRj9LJDXWoM/IgQdiZIf11XlSkR1Y8bsmRy");
		AmazonDynamoDBClient ddb = new AmazonDynamoDBClient(basic);
		ddb.setRegion(Region.getRegion(Regions.US_WEST_2));
		//final AmazonDynamoDBClient ddb = new AmazonDynamoDBClient();
		DynamoDB dynamodb = new DynamoDB(ddb);
		for(Map<String, String> outputItem : outputList) {
			final String url = outputItem.get("UrlName");
			final Map<String, AttributeValue> keyValue = new HashMap<>();
			keyValue.put("UrlName", new AttributeValue(url));
    		final GetItemRequest request = new GetItemRequest()
				                       .withKey(keyValue)
				                       .withTableName(tableName);
    		try {
	    		final Map<String,AttributeValue> returned_item = ddb.getItem(request).getItem();
	    		// if url is in the db, get the rank
    			if (returned_item != null) {
	    			Set<String> keys = returned_item.keySet();
		    		for (String key : keys) {
			    		if(key.equals("UrlRank")) {
			    			outputItem.put("rank", returned_item.get(key).getN());
					    }
				    }
		    	// if url is not in the database, calculate baseline rank and add it to the database
    			} else {
    				// calculate baseline rank
    				final BaselineUrlChecker baselineUrlChecker = new BaselineUrlChecker(url);
	    			final int rank = baselineUrlChecker.getBaselineUrlRank();
	    			// add url to db
		    		final Table table = dynamodb.getTable(tableName);
			    	final Item item = new Item().withPrimaryKey("UrlName", url).withNumber("UrlRank", rank);
				    PutItemOutcome outcome = table.putItem(item);
	    			outputItem.put("rank", Integer.toString(rank));
    			}
	    	} catch (AmazonServiceException e) {
		    	// database read failed for some reason
			    throw new RankCalculationFailureException("Failure to read URL database." +e.getErrorMessage());
		    }

		}
		
		return outputList;
    }
}
