package validator;

class RankCalculationFailureException extends Exception {
	private static final long serialVersionUID = 1L;
	RankCalculationFailureException() {
		super("Failed to caluclate rank");
	}
	RankCalculationFailureException(String message) {
		super("Failed to calculate rank: " + message);
	}

}
